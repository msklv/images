//
//  StructHolder.swift
//  121
//
//  Created by Павел Москалев on 9/10/21.
//

import Foundation

class StructHolder<T>: NSObject {
    let savedStruct: T
    init(savedStruct: T) {
        self.savedStruct = savedStruct
    }
}
