//
//  Caching.swift
//  121
//
//  Created by Павел Москалев on 9/10/21.
//

import Foundation

class Caching {
    
    static let shared = Caching()
    let cache = NSCache<NSString, StructHolder<Any>>()
    
    private init() { }
    
    func getObject<T>(forKey key: NSString) -> T? {
        guard let cachedObject = cache.object(forKey: key) else { return nil }
        return cachedObject.savedStruct as? T
    }
    
    func setObject<T>(object: T, forKey key: NSString) {
        cache.setObject(StructHolder(savedStruct: object), forKey: key)
    }
    
    func cacheRemoveAllObjects()
    {
        cache.removeAllObjects()
    }
}

