//
//  ImageCollectionViewCell.swift
//  121
//
//  Created by Павел Москалев on 9/10/21.
//

import UIKit
import Kingfisher
import Combine

class ImageCollectionViewCell: UICollectionViewCell {
    
    static let nib = "ImageCollectionViewCell"
    static let identifier = "tableCell"
    
    let url = URL(string: "https://loremflickr.com/200/200/")!
    private var subscriptions = Set<AnyCancellable>()
    
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        ImageView.image = nil
    }
    
    func configure(item: String) {
        ImageView.layer.cornerRadius = 7
        ImageView.backgroundColor = .systemGray5
        load(imageForKey: item)
    }
    
    func load(imageForKey key: String) {
        activityIndicator.startAnimating()
        if let cachedImage: UIImage = Caching.shared.getObject(forKey: key as NSString) {
            self.ImageView.image = cachedImage
            self.activityIndicator.stopAnimating()
        } else {
            fetchImage()
                .receive(on: DispatchQueue.main)
                .sink { [weak self] completion in
                    guard case .failure(let error) = completion else { return }
                    self?.activityIndicator.stopAnimating()
                    print(error)
                } receiveValue: { [weak self] image in
                    Caching.shared.setObject(object: image,
                                             forKey: key as NSString)
                    self?.ImageView.image = image
                    self?.activityIndicator.stopAnimating()
                }
                .store(in: &subscriptions)
        }
    }
    
    func fetchImage() -> AnyPublisher<UIImage?, Never> {
        return URLSession.shared
            .dataTaskPublisher(for: url)
            .map { data, _ in UIImage(data: data) }
            .replaceError(with: nil)
            .eraseToAnyPublisher()
    }
}
