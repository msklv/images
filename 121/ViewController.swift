//
//  ViewController.swift
//  121
//
//  Created by Павел Москалев on 9/10/21.
//

import UIKit
//import Kingfisher

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var barButton : UIBarButtonItem?
    var refreshButton : UIBarButtonItem?

    var count = 140
  
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        navigationItem.title = "Picture"
      setupUI()
    }
    
   func setupUI()
   {
    self.barButton = UIBarButtonItem(title: "NewPicture", style: .plain, target: self, action: #selector(add))
        self.navigationItem.rightBarButtonItems = [barButton] as? [UIBarButtonItem]
    self.refreshButton = UIBarButtonItem(title: "Refresh", style: .plain, target: self, action: #selector(clearCache))
        self.navigationItem.leftBarButtonItems = [refreshButton] as? [UIBarButtonItem]
   }
    
    @objc func clearCache()
    {
        Caching.shared.cacheRemoveAllObjects()
        count = 140
        collectionView.reloadData()
    }
    
    @objc func add()
    {
        count += 1
            let indexPath = IndexPath(row:  count - 1 , section: 0)
            collectionView.insertItems(at: [indexPath])
    }
    
    func configureCollectionView()
    {
        collectionView.collectionViewLayout = createCompositionalLayout()
        collectionView.register(UINib(nibName: ImageCollectionViewCell.nib, bundle: nil),
                                forCellWithReuseIdentifier: ImageCollectionViewCell.identifier)
    }
    
    private func createCompositionalLayout() -> UICollectionViewCompositionalLayout {
        return UICollectionViewCompositionalLayout { (_ , _) -> NSCollectionLayoutSection? in
            return self.screenLayout()
        }
    }
    
    private func screenLayout() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1/7), heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = .init(top: 1, leading: 1, bottom: 1, trailing: 1)
        
        let rowGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1/10))
        let rowGroup = NSCollectionLayoutGroup.horizontal(layoutSize: rowGroupSize, subitems: [item])
        let externalGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        let externalGroup = NSCollectionLayoutGroup.vertical(layoutSize: externalGroupSize, subitems: [rowGroup])
        
        let section = NSCollectionLayoutSection(group: externalGroup)
        section.orthogonalScrollingBehavior = .continuous
        return section
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: indexPath) as? ImageCollectionViewCell else { return UICollectionViewCell() }
        cell.configure(item: "\(indexPath.item)")
        return cell
    }
}

